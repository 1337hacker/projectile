## Használat

A röppálya kezdeti értékeit illetve az idő léptéket a program futtatásakor lehet változtatni. Amennyiben a célpont geometriáját (ami egy háromszöglehet csak) szeretnénk változtatni, akkor a forráskódban kell módosítani a "start" "stop" illetve a "peak" paraméterek értékeit.

A "start" paraméter adja meg a hegy lábának a távolságát, "stop" pedig a hegy legtávolabbi végét. a "peak" struktura pedig a hegy legmagasabb pontjának a koordinátája (x,y).

```bash
go build main.go 
```
```bash
./main kezdetiSebesség kilövésiSzög időLépték 
```

## Optimalizációs lehetőségek

A lehetséges kilövésiSzög értékek szögfüggvényeit egy look up táblában tárolni.
Egyszer meghívott függvények definiálása helyett main()-ben végezni el a számításokat.
If elágazások csökkenthetőek lennének, ha a kezdeti érték paraméterekből nem két egyenes egyenletet generálok, hanem kettő vonalat.
Analitikusan kiszámítani a röppálya végét, és csak addig a pontig szimulálni.
Párhuzamosítás jelen esetben go routine-okkal (vagy threadekkel):
röppálya kiszámítjuk, majd ezen a tömbön több helyen elindítjuk a becsapódás kereső függvényt

## Tesztelési ötletek

Mért értékek segítségével kalibráljuk a modellt teszt környezetben.
Függvények tesztelése unit teszttel. Megadjuk, hogy adott inputra milyen outputot várunk.
Nyelvi szabvány implementációja.
paraméter dimenziójának ellenőrzése.


## Pontatlanság lehetséges okai
Túl egyszerű a modell, nem veszi figyelembe a légellenállást, szelet pl.
Az időlépték túl kicsi.
