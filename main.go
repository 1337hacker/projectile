package main

import (
	"fmt"
	"math"
    "os"
    "strconv"
    "log"
)

type point struct {
	x float64
	y float64
}
type traj struct {
	t float64
	point
}

type line struct {
	m   float64
	y_0 float64
}
//Calculates the mountains faces from the initial conditions (start stop and peak)
func (l *line) calcParams(x float64, peak point){
    l.y_0=peak.y/(1-peak.x/x)
    l.m=-l.y_0/x
}
//Calculates the x and y coordinates of the projectile
func (p *point) getPoint(t float64) {
	p.x = v_0 * t * math.Cos(fi*(math.Pi/180))
	p.y = v_0*t*math.Sin(fi*(math.Pi/180)) - 0.5*g*t*t
}
//Check if the projectile collided in the given coordinate. Mountain was given as 2 line, so it has to be trimmed
func checkBumm(loc traj) (bumm bool) {
    var y float64
    if loc.x<start || loc.x>stop{
        y=0
    }else if loc.x < peak.x {
        y = front.m*loc.x + front.y_0
	}else if loc.x> peak.x{
        y = back.m*loc.x + back.y_0
	}
	if y < 0 {
		y = 0
	}
	if loc.y > y {
		bumm = false
	} else {
	    bumm = true
	}
    fmt.Println(loc.x,loc.y,y)
    return bumm
}

var g float64 = 9.81
var v_0 float64
var fi float64
var front line
var back line
//initial conditions hard coded
var start float64= 2000
var stop float64=5000
var peak = point{x:3500,y:3000}
func main() {
	var bumm bool = false
    arguments := os.Args[1:]
    var numbers []float64
    //reads in and converts input from string to float nozzle speed, angle, timestep
    for _,number := range arguments {
        number, err :=strconv.ParseFloat(number,64)
        if err != nil {
            log.Fatal(err)
        }
        numbers = append(numbers, number)
    }
    v_0 = numbers[0]
    fi = numbers[1]
    t := numbers[2]
    front.calcParams(start,peak)
    back.calcParams(stop,peak)
	var loc traj
	locs := []traj{}
	for !bumm {
		loc.getPoint(t)
        loc.t=t
		locs = append(locs, loc)
		bumm = checkBumm(loc)
		t = t + numbers[2]

	}
    fmt.Println()
    if locs[len(locs)-1].x<start || locs[len(locs)-1].x>stop {
        fmt.Printf("Projectile missed the mountain, landed on: [%0.2f;%0.2f] at %0.2f sec",locs[len(locs)-1].x,locs[len(locs)-1].y,locs[len(locs)-1].t)
    }else{
        fmt.Printf("Target got hit, landed on: [%0.2f;%0.2f] at %0.2f sec",locs[len(locs)-1].x,locs[len(locs)-1].y,locs[len(locs)-1].t)
    }
}
